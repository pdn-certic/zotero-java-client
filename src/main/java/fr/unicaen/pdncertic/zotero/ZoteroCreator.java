package fr.unicaen.pdncertic.zotero;

public class ZoteroCreator {


    private String creatorType;
    private String localized;
    private String firstName;
    private String lastName;
    private String name;

    public ZoteroCreator(String type, String firstName, String lastName){
        this.creatorType = type;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getCreatorType() {
        return creatorType;
    }

    public String getLocalized() {
        return localized;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return name;
    }

    public void setCreatorType(String creatorType) {
        this.creatorType = creatorType;
    }

    public void setLocalized(String localized) {
        this.localized = localized;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setName(String name) {
        this.name = name;
    }
}