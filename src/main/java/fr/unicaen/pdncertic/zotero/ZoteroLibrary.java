package fr.unicaen.pdncertic.zotero;

public class ZoteroLibrary {

    public enum LibraryType {
        groups,
        users
    };

    private String apiKey;
    private String id;
    private LibraryType type;

    public ZoteroLibrary(LibraryType type, String id){
        this(type, id, null);
    }
    public ZoteroLibrary(LibraryType type, String id, String apiKey){
        this.type = type;
        this.id = id;
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getId() {
        return id;
    }

    public LibraryType getType() {
        return type;
    }

}
