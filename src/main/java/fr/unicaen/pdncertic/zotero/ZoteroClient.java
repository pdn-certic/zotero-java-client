package fr.unicaen.pdncertic.zotero;

import com.fasterxml.jackson.core.type.TypeReference;
import kong.unirest.*;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONException;
import kong.unirest.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ZoteroClient {

    private static String API_URL = "https://api.zotero.org/";

    public static List<ZoteroItem> search(ZoteroLibrary library, String query) throws ZoteroClientException {
        try {
            List<ZoteroItem> items = new ArrayList<>();
            String url = API_URL + library.getType().toString() + "/" + library.getId() + "/items?q=" + query;
            GetRequest getRequest = Unirest.get(url);
            if (library.getApiKey() != null)
                getRequest = getRequest.header("Authorization", "Bearer " + library.getApiKey());
            HttpResponse<JsonNode> response = getRequest.asJson();
            if (response.getStatus() == 200) {
                JSONArray jsonResponseArray = response.getBody().getArray();
                for (int i = 0; i < jsonResponseArray.length(); i++) {
                    String href = jsonResponseArray.getJSONObject(i).getJSONObject("links").getJSONObject("self").getString("href");
                    //alternate href = url vers la fiche zotero en ligne
                    String alternateHref = jsonResponseArray.getJSONObject(i).getJSONObject("links").getJSONObject("alternate").getString("href");
                    JSONObject dataObject = jsonResponseArray.getJSONObject(i).getJSONObject("data");
                    ObjectMapper mapper = new ObjectMapper();
                    Map<String, Object> map = mapper.readValue(dataObject.toString(), new TypeReference<Map<String, Object>>(){});
                    String key = readDataValue("key", dataObject);
                    ZoteroItem currentItem = new ZoteroItem(key, href, alternateHref, map, library);
                    items.add(currentItem);
                }
                return items;
            } else {
                throw new ZoteroClientException("");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ZoteroClientException(e.getMessage());
        }

    }

    public static String readDataValue(String key, JSONObject dataObject){
        try {
            return dataObject.get(key).toString();
        }
        catch(JSONException e){
            return null;
        }
    }

    /** tests **/
    public static void main(String[] args){
        fr.unicaen.pdncertic.zotero.ZoteroLibrary[] libs = {
                new fr.unicaen.pdncertic.zotero.ZoteroLibrary(fr.unicaen.pdncertic.zotero.ZoteroLibrary.LibraryType.users, "475425"),
                new fr.unicaen.pdncertic.zotero.ZoteroLibrary(fr.unicaen.pdncertic.zotero.ZoteroLibrary.LibraryType.groups, "447270"),
                new fr.unicaen.pdncertic.zotero.ZoteroLibrary(fr.unicaen.pdncertic.zotero.ZoteroLibrary.LibraryType.groups, "427575")
        };
        List<fr.unicaen.pdncertic.zotero.ZoteroItem> items = null;
        for(int i = 0; i < libs.length; i++) {
            fr.unicaen.pdncertic.zotero.ZoteroLibrary lib = libs[i];
            try {
                items = fr.unicaen.pdncertic.zotero.ZoteroClient.search(lib, "e");
            } catch (fr.unicaen.pdncertic.zotero.ZoteroClientException e) {
                e.printStackTrace();
            }
            for (fr.unicaen.pdncertic.zotero.ZoteroItem it : items) {
                System.err.println(it.getKey() + " -> " + it.getDataValue("publisher")+"/"+ it.getDataValue("date") + "/" + it.getAlternateHref());
            }
        }
    }

}
