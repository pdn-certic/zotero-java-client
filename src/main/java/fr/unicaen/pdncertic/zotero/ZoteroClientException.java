package fr.unicaen.pdncertic.zotero;

public class ZoteroClientException extends Exception {

    public ZoteroClientException(String message){
        super(message);
    }
}
