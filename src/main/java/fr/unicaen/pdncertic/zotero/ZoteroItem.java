package fr.unicaen.pdncertic.zotero;

import java.util.Map;

public class ZoteroItem {

    private String key;
    private ZoteroLibrary library;
    private Map<String, Object> dataMap;
    private String href;
    private String alternateHref;

    public ZoteroItem(String key, String href, String alternateHref, Map<String, Object> dataMap, ZoteroLibrary library){
        this.key = key;
        this.href = href;
        this.alternateHref = alternateHref;
        this.dataMap = dataMap;
        this.library = library;
    }

    public String getKey() {
        return this.key;
    }

    public String getHref() {
        return this.href;
    }

    public String getAlternateHref() {
        return this.alternateHref;
    }

    public ZoteroLibrary getLibrary() {
        return library;
    }

    public void setLibrary(ZoteroLibrary library) {
        this.library = library;
    }


    public Map<String, Object> getDataMap(){
        return this.dataMap;
    }

    public Object getDataValue(String key){
        return this.dataMap.get(key);
    }
}
