# zotero-java-client

Client Java très basique (recherche uniquement) d'interrogation de l'API Zotero.

## Compilation & installation locale

```
mvn package
mvn install
```


## Exemple

```
ZoteroLibrary lib = new ZoteroLibrary(fr.unicaen.pdncertic.zotero.ZoteroLibrary.LibraryType.groups, "427575");
List<fr.unicaen.pdncertic.zotero.ZoteroItem> items = fr.unicaen.pdncertic.zotero.ZoteroClient.search(lib, "medieval");
```